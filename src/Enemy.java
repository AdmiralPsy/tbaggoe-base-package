import java.util.List;

/**
 * A subclass of character representing anything that the hero battles.
 */
class Enemy extends Character {
    private final int xpValue;

    /**
     * Creates an enemy character with a collection of data. Typically used as a superclass for enemy classes.
     *
     * @param name the name of the enemy
     * @param deck a list containing the enemy's cards
     * @param hp the enemy's starting hp
     * @param xpValue the xp gained from defeating the enemy
     */
    Enemy(String name, List<Card> deck, int hp, int xpValue) {
        super(name, hp, deck);
        this.xpValue = xpValue;
    }

    /**
     * Returns the xp value of the enemy.
     *
     * @return the xp value
     */
    public int getXpValue() {
        return this.xpValue;
    }

    /**
     * Finds the enemy's best damage card and plays it.
     *
     * @return the enemy's damage card
     */
    public Card playBestDamage() {
        int selectedCardIndex = 0;
        for (int i = 0; i < this.getHand().size(); i++) {
            if (this.getHand().get(i).getDamage() > this.getHand().get(selectedCardIndex).getDamage()) {
                selectedCardIndex = i;
            }
        }
        return this.playCard(selectedCardIndex);
    }

    /**
     * Finds the enemy's best guard card and plays it.
     *
     * @return the enemy's guard card
     */
    public Card playBestGuard() {
        int selectedCardIndex = 0;
        for (int i = 0; i < this.getHand().size(); i++) {
            if (this.getHand().get(i).getGuard() > this.getHand().get(selectedCardIndex).getGuard()) {
                selectedCardIndex = i;
            }
        }
        return this.playCard(selectedCardIndex);
    }
}
