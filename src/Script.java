/**
 * A static class that variables containing scripts for the game are stored in.
 */
class Script {
    // Requests
    protected static final String NAME_REQUEST = "What is the hero's name? ";
    protected static final String DECK_REQUEST = "Would the hero like the berserker (0), tactician (1), or tank (2) deck? ";
    protected static final String PREP_QUIT = "Press enter to quit ";

    // Healing
    protected static final String RESTORED_HEALTH = "%s has their health restored\n";

    // Battle Script
    protected static final String DAMAGE_CARD_CHOICE = "%s picks a card to attack";
    protected static final String GUARD_CARD_CHOICE = "%s picks a card to guard";
    protected static final String CARD_LIST = "Current Hand:";
    protected static final String DAMAGE_CHOICE = "%s attacks with %s";
    protected static final String GUARD_CHOICE = "%s guards with %s";
    protected static final String CARD_REQUEST = "What card would %s like to play? ";
    protected static final String TAKE_DAMAGE = "%s takes %s damage";
    protected static final String REMAINING_HEALTH = "%s has %s health points remaining";
    protected static final String ENGAGE = "%s has engaged %s";
    protected static final String DEFEAT_ENEMY = "%s defeats %s";
    protected static final String XP_GAIN = "%s gains %s experience points";
    protected static final String DIE = "%s dies";

    // Store Cards
    protected static final String CARDS_IN_DECK = "%s has %s cards out of 10 allowed in their deck";
    protected static final String REMOVE_CARD_REQUEST = "Which card would %s like to take out of their deck? ";

    // Level Up
    protected static final String LEVEL_UP = "%s leveled up!";
    protected static final String NEW_HP = "%s now has %s health points";

    // Saving
    protected static final String SAVING = "Saving the game";
    protected static final String SAVED = "Game saved";

    // Loading
    protected static final String NO_LOAD_POSSIBLE = "There is no game to be loaded";
    protected static final String LOADING = "Loading previous game";
    protected static final String LOADED = "Game loaded";
}
