import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class HeroTest {
    @Test
    public void testLoadHeroInfo() throws Exception {
        // Create sample hero file
        File file = new File("TestHeroData.txt");
        FileWriter writer = new FileWriter(file);
        writer.write("Gerald\n");
        writer.write("27\n");
        writer.write("81\n");
        writer.write("42\n");
        writer.write("5\n");
        writer.write("Fred`and`John1/2\n");
        writer.write("Wilma3/4\n");
        writer.write("Barney5/6\n");
        writer.write("Betty7/8\n");
        writer.flush();
        writer.close();
        // Create a new hero and load the hero info
        Hero h = new Hero("Name", new ArrayList<>());
        assertTrue(h.loadHeroInfo("TestHeroData.txt"));
        // Test hero attributes
        assertEquals(h.getName(), "Gerald");
        assertEquals(h.getHp(), 27);
        Field maxHp = h.getClass().getDeclaredField("maxHp");
        maxHp.setAccessible(true);
        assertEquals("Checking maxHp", maxHp.get(h), 81);
        Field xp = h.getClass().getDeclaredField("xp");
        xp.setAccessible(true);
        assertEquals("Checking maxHp", xp.get(h), 42);
        // Assembles and tests the contents of the deck.
        List<Card> heroDeck = h.getFullDeck();
        assertEquals("Deck size should be 4", heroDeck.size(), 4);
        assertEquals("First card should be Fred and John (1/2)", heroDeck.get(0).toString(), "Fred and John (1/2)");
        assertEquals("Second card should be Wilma (1/2)", heroDeck.get(1).toString(), "Wilma (3/4)");
        assertEquals("Third card should be Barney (1/2)", heroDeck.get(2).toString(), "Barney (5/6)");
        assertEquals("Fourth card should be Betty (1/2)", heroDeck.get(3).toString(), "Betty (7/8)");
        // Delete test save file.
        assertTrue("Trying to delete file", file.delete());
    }
}