public class Card {
    private final String name;
    private final int damage;
    private final int guard;

    /**
     * Creates the card object.
     *
     * @param name the card's name
     * @param damage the card's damage value
     * @param guard the card's guard value
     */
    public Card(String name, int damage, int guard) {
        this.name = name;
        this.damage = damage;
        this.guard = guard;
    }

    /**
     * Returns the name of the card.
     *
     * @return the card's name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns the card's damage value
     *
     * @return the card's damage value
     */
    public int getDamage() {
        return this.damage;
    }

    /**
     * Returns the card's guard value
     *
     * @return the card's guard value
     */
    public int getGuard() {
        return this.guard;
    }

    /**
     * Returns a string interpretation of all of the card's info.
     *
     * @return the card's info
     */
    public String toString() {
        return this.name + " " + "(" + Integer.toString(this.damage) + "/" + Integer.toString(this.guard) + ")";
    }
}
