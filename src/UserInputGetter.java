import java.util.Scanner;

/**
 * Gets input from the user. Runs statically.
 */
class UserInputGetter {
    /**
     * Can take any number of int inputs from the user. If none, any number will be gotten from the user. If 1, any number below the given. If 2, any number between those. If three or more, reverts to 0.
     *
     * @return the user's number
     */
    public static int getNumberInput(int... limits) {
        Scanner scanner = new Scanner(System.in);
        String userInput;
        Integer userInt;
        userInt = null;
        while (userInt == null) {
            try {
                userInput = scanner.nextLine();
                userInt = Integer.parseInt(userInput);
                switch (limits.length) {
                    case 1:
                        if (userInt >= limits[0]) {
                            userInt = null;
                        }
                        break;
                    case 2:
                        if (limits[0] > userInt || userInt >= limits[1]) {
                            userInt = null;
                        }
                    default:
                        break;
                }
            } catch (Exception e) {
                // Do nothing
            }
        }
        return userInt;
    }

    /**
     * Gets a string from the player.
     *
     * @return the user's string
     */
    public static String getStringInput() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
