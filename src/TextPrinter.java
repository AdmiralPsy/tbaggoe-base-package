/**
 * Takes strings and prints them to the console with varying delays. Runs statically.
 */
class TextPrinter {
    /**
     * Prints a string with a short delay.
     *
     * @param text the string to be printed
     */
    public static void pausedPrint(String text) {
        System.out.println(text);
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            // Do nothing
        }
    }

    /**
     * Prints a string with a long delay.
     *
     * @param text the string to be printed
     */
    public static void longPausedPrint(String text) {
        System.out.println(text);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            // Do nothing
        }
    }

    /**
     * Prints a string with no delay and no line break.
     *
     * @param text the string to be printed
     */
    public static void requestPrint(String text) {
        System.out.print(text);
    }
}
