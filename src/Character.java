import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A character in the game, possessing a deck of cards and an hp stat. Can be the hero or enemy of the player.
 */
class Character {
    protected String name;
    int hp;
    @SuppressWarnings("CanBeFinal")
    List<Card> fullDeck;
    private List<Card> workingDeck;
    private List<Card> hand;

    /**
     * Creates a character object.
     *
     * @param name the name of the player
     * @param hp the health of the player
     * @param deck the cards of the player
     */
    protected Character(String name, int hp, List<Card> deck) {
        this.name = name;
        this.hp = hp;
        this.fullDeck = deck;
        this.workingDeck = new ArrayList<>();
        this.hand = new ArrayList<>();
    }

    /**
     * Gets the character's name.
     *
     * @return the character's name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the character's health points.
     *
     * @return the character's hp
     */
    public int getHp() {
        return this.hp;
    }

    /**
     * Returns a list with every card in the character's deck.
     * @return all of the player's deck
     */
    List<Card> getFullDeck() {
        return this.fullDeck;
    }

    /**
     * Returns a list of the character's five-card hand.
     *
     * @return the player's hand
     */
    public List<Card> getHand() {
        return this.hand;
    }

    /**
     * Shuffles the full deck and makes that a new working deck.
     */
    void shuffleDeck() {
        this.workingDeck = new ArrayList<>();
        this.workingDeck.addAll(this.fullDeck);
        Collections.shuffle(this.workingDeck);
    }

    /**
     * Takes the first card in the working deck and moves it to the hand.
     */
    public void drawCard() {
        if (this.workingDeck.size() == 0) {
            this.shuffleDeck();
        }
        this.hand.add(this.workingDeck.get(0));
        this.workingDeck.remove(0);
    }

    /**
     * Empties the character's hand draws five cards.
     */
    public void newHand() {
        this.hand = new ArrayList<>();
        while (this.hand.size() < 5) {
            this.drawCard();
        }
    }

    /**
     * Takes a damage value and subtracts it from the character's hp, setting it to zero if it falls below that.
     *
     * @param damageValue the damage taken
     */
    public void takeDamage(int damageValue) {
        TextPrinter.pausedPrint(String.format(Script.TAKE_DAMAGE, this.getName(), damageValue));
        this.hp -= damageValue;
        if (this.hp < 0) {
            this.hp = 0;
        }
    }

    /**
     * Takes an index for a card from the hand, removes it from the hand, and returns the card
     *
     * @param handIndex the index of the desired card
     * @return the desired card
     */
    public Card playCard(int handIndex) {
        Card chosenCard = this.hand.get(handIndex);
        this.hand.remove(handIndex);
        return chosenCard;
    }
}
