/**
 * Takes a hero and enemy and simulates a battle between them. Runs statically.
 */
class Battle {

    /**
     * Causes both characters to run their function to get a new hand.
     *
     * @param hero the hero character
     * @param enemy the enemy character.
     */
    private static void drawStartingHands(Hero hero, Enemy enemy) {
        hero.newHand();
        enemy.newHand();
    }

    /**
     * Causes the enemy character to play a damage card.
     *
     * @param enemy the enemy character
     * @return the damage card
     */
    private static Card enemyDamageCard(Enemy enemy) {
        Card chosenCard = enemy.playBestDamage();
        enemy.drawCard();
        TextPrinter.pausedPrint(String.format(Script.DAMAGE_CHOICE, enemy.getName(), chosenCard.toString()));
        System.out.println("");
        return chosenCard;
    }

    /**
     * Causes the enemy character to play a guard card.
     *
     * @param enemy the enemy character
     * @return the guard card
     */
    private static Card enemyGuardCard(Enemy enemy) {
        Card chosenCard = enemy.playBestGuard();
        enemy.drawCard();
        TextPrinter.pausedPrint(String.format(Script.GUARD_CHOICE, enemy.getName(), chosenCard.toString()));
        System.out.println("");
        return chosenCard;
    }

    /**
     * Requests the player to choose a damage card for the hero, and plays that card.
     *
     * @param hero the hero character
     * @return the damage card
     */
    private static Card heroDamageCard(Hero hero) {
        TextPrinter.pausedPrint(String.format(Script.DAMAGE_CARD_CHOICE, hero.getName()));
        TextPrinter.pausedPrint(Script.CARD_LIST);
        for (int cardIndex = 0; cardIndex < hero.getHand().size(); cardIndex++) {
            TextPrinter.pausedPrint("(" + Integer.toString(cardIndex) + ") " + hero.getHand().get(cardIndex).toString());
        }

        TextPrinter.requestPrint(String.format(Script.CARD_REQUEST, hero.getName()));
        Card chosenCard = hero.playCard(UserInputGetter.getNumberInput(0, hero.getHand().size()));
        hero.drawCard();
        TextPrinter.pausedPrint(String.format(Script.DAMAGE_CHOICE, hero.getName(), chosenCard.getName()));
        System.out.println("");
        return chosenCard;
    }

    /**
     * Requests the player to choose a guard card for the hero, and plays that card.
     *
     * @param hero the hero character
     * @return the guard card
     */
    private static Card heroGuardCard(Hero hero) {
        TextPrinter.pausedPrint(String.format(Script.GUARD_CARD_CHOICE, hero.getName()));
        TextPrinter.pausedPrint(Script.CARD_LIST);
        for (int cardIndex = 0; cardIndex < hero.getHand().size(); cardIndex++) {
            TextPrinter.pausedPrint("(" + Integer.toString(cardIndex) + ") " + hero.getHand().get(cardIndex).toString());
        }

        TextPrinter.requestPrint(String.format(Script.CARD_REQUEST, hero.getName()));
        Card chosenCard = hero.playCard(UserInputGetter.getNumberInput(0, hero.getHand().size()));
        hero.drawCard();
        TextPrinter.pausedPrint(String.format(Script.GUARD_CHOICE, hero.getName(), chosenCard.getName()));
        System.out.println("");
        return chosenCard;
    }

    /**
     * Gets damage and guard cards for the hero and enemy, and uses them to calculate damage.
     *
     * @param hero the hero character
     * @param enemy the enemy character
     */
    private static void runRound(Hero hero, Enemy enemy) {

        Card enemyDamageCard = Battle.enemyDamageCard(enemy);
        Card heroGuardCard = Battle.heroGuardCard(hero);
        Card heroDamageCard = Battle.heroDamageCard(hero);
        Card enemyGuardCard = Battle.enemyGuardCard(enemy);

        int enemyHpLoss = heroDamageCard.getDamage() - enemyGuardCard.getGuard();
        if (enemyHpLoss < 0) {
            enemyHpLoss = 0;
        }
        int heroHpLoss = enemyDamageCard.getDamage() - heroGuardCard.getGuard();
        if (heroHpLoss < 0) {
            heroHpLoss = 0;
        }

        hero.takeDamage(heroHpLoss);
        enemy.takeDamage(enemyHpLoss);

        TextPrinter.pausedPrint(String.format(Script.REMAINING_HEALTH, hero.getName(), hero.getHp()));
        TextPrinter.pausedPrint(String.format(Script.REMAINING_HEALTH, enemy.getName(), enemy.getHp()));
    }

    /**
     * Repeatedly uses the function to run a round until one of the characters has died.
     *
     * @param hero the hero character
     * @param enemy the enemy character
     */
    public static void runBattle(Hero hero, Enemy enemy) throws Exception {
        TextPrinter.pausedPrint(String.format(Script.ENGAGE, enemy.getName(), hero.getName()));
        Battle.drawStartingHands(hero, enemy);

        while (hero.getHp() > 0 && enemy.getHp() > 0) {
            Battle.runRound(hero, enemy);
        }

        if (hero.getHp() == 0) {
            TextPrinter.longPausedPrint(String.format(Script.DIE, hero.getName()));
            TextPrinter.requestPrint(Script.PREP_QUIT);
            UserInputGetter.getStringInput();
            System.exit(0);
        } else {
            TextPrinter.pausedPrint(String.format(Script.DEFEAT_ENEMY, hero.getName(), enemy.getName()));
            TextPrinter.pausedPrint(
                    String.format(Script.XP_GAIN, hero.getName(), Integer.toString(enemy.getXpValue())));
            hero.gainXp(enemy.getXpValue());
            System.out.println("");
        }
    }
}