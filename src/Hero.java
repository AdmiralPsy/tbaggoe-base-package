import java.io.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The hero of the game, controlled by the player.
 */
class Hero extends Character {
    private int maxHp;
    private int xp;
    private int progress;

    /**
     * Creates the hero object based on the character object.
     *
     * @param name the name of the hero
     * @param deck the hero's deck
     */
    public Hero(String name, List<Card> deck) {
        super(name, 10, deck);
        this.maxHp = 10;
        this.progress = 0;
    }


    /**
     * Lists all of the hero's cards and has the player select cards to be removed.
     * Call this function whenever the hero gains cards, but after all types of cards have been assigned.
     */
    public void fixDeckSize() {
        // Loops the script until the deck is the correct size.
        while (this.getFullDeck().size() > 10) {
            // Prints the "Cards in Deck" script.
            TextPrinter.pausedPrint(String.format(
                            Script.CARDS_IN_DECK, this.getName(), this.getFullDeck().size())
            );
            // Prints the index of each card and the card's string interpretation.
            for (int i = 0; i < this.getFullDeck().size(); i++) {
                TextPrinter.pausedPrint("(" + Integer.toString(i) + ") " + this.getFullDeck().get(i).toString());
            }
            // Prints the "Remove Card Request" script.
            TextPrinter.pausedPrint(String.format(Script.REMOVE_CARD_REQUEST, this.getName()));
            // Gets an integer representing the card to be removed from the player, and removes the corresponding card.
            this.removeCardFromDeck(this.getFullDeck().get(UserInputGetter.getNumberInput(0, this.getFullDeck().size())));
            // Shuffles the deck.
            this.shuffleDeck();
            // Prints a blank line.
            System.out.println("");
        }
    }

    /**
     * Takes a card and a number and puts that many cards in the hero's deck.
     *
     * @param cardAdded the card to be added to the deck
     * @param numberOfCards the number of cards to add
     */
    public void addCards(Card cardAdded, int numberOfCards) {
        for (int i = 0; i < numberOfCards; i++) {
            this.addCardToDeck(cardAdded);
        }
    }

    /**
     * Takes a card and a number and removes that many cards from the hero's deck.
     *
     * @param cardRemoved the card to be removed from the deck
     * @param numberOfCards the number of cards to be removed
     */
    public void removeCards(Card cardRemoved, int numberOfCards) {
        for (int i = 0; i < numberOfCards; i++) {
            if (this.fullDeck.contains(cardRemoved)) {
                this.removeCardFromDeck(cardRemoved);
            } else {
                break;
            }
        }
    }

    /**
     * Sets the hero's health to the hero's maximum health.
     */
    public void resetHp() {
        TextPrinter.pausedPrint(String.format(Script.RESTORED_HEALTH, this.getName()));
        this.hp = maxHp;
    }

    /**
     * Increases the hero's xp level and deals with any level ups.
     *
     * @param xpAmount the change in the hero's xp
     */
    public void gainXp(int xpAmount) {
        // Create an instance of the script object.
        this.xp += xpAmount;
        int hpXpTotal = this.xp / 5 + 10;
        if (hpXpTotal > this.maxHp) {
            TextPrinter.pausedPrint(String.format(Script.LEVEL_UP, this.getName()));
            TextPrinter.pausedPrint(String.format(Script.NEW_HP, this.getName(), hpXpTotal));
            this.maxHp = hpXpTotal;
        }
    }

    /**
     * Deletes a card from the hero's deck.
     *
     * @param card the card to be deleted
     */
    private void removeCardFromDeck(Card card) {
        this.fullDeck.remove(card);
    }

    /**
     * Adds a card to the hero's deck
     *
     * @param card the card to be added
     */
    private void addCardToDeck(Card card) {
        this.fullDeck.add(card);
    }

    /**
     * Takes all of the information relevant to the current playthrough and writes it to the save file.
     *
     * @param fileName the name of the save file
     * @throws IOException
     */
    public void saveHeroInfo(String fileName) throws IOException {
        TextPrinter.pausedPrint(Script.SAVING);

        FileWriter writer = new FileWriter(fileName);
        writer.write(this.name + "\n");
        writer.write(Integer.toString(this.hp) + "\n");
        writer.write(Integer.toString(this.maxHp) + "\n");
        writer.write(Integer.toString(this.xp) + "\n");
        writer.write(Integer.toString(this.progress) + "\n");
        for (Card card : this.fullDeck) {
            writer.write(this.encodeSpaces(card.getName()) + Integer.toString(card.getDamage()) + "/" +
                    Integer.toString(card.getGuard()) + "\n");
        }
        writer.close();

        TextPrinter.pausedPrint(Script.SAVED);
        System.out.println("");
    }

    /**
     * Takes a string and returns the string with all spaces turned into `'s.
     *
     * @param original the string with spaces
     * @return the string with `'s
     */
    private String encodeSpaces(String original) {
        return original.replaceAll(" ", "`");
    }

    /**
     * Takes the save file, pulls out any information inside, and applies it to the hero.
     *
     * @param fileName the name of the save file
     * @return whether there was a file to be loaded
     * @throws IOException
     */
    public boolean loadHeroInfo(String fileName) throws IOException {
        TextPrinter.pausedPrint(Script.LOADING);

        String patternString = "^([A-Za-z/`]+)(\\d+)/(\\d+)$";
        Pattern pattern = Pattern.compile(patternString);
        FileReader reader = new FileReader(fileName);
        BufferedReader bufferedReader = new BufferedReader(reader);
        this.name = bufferedReader.readLine();
        if (!(this.name == null)) {
            try {
                this.hp = Integer.parseInt(bufferedReader.readLine());
                this.maxHp = Integer.parseInt(bufferedReader.readLine());
                this.xp = Integer.parseInt(bufferedReader.readLine());
                this.progress = Integer.parseInt(bufferedReader.readLine());
            } catch (Exception exception) {
                TextPrinter.pausedPrint("There was an error loading the game");
                TextPrinter.pausedPrint("Please contact whoever created this crap code");
                return false;
            }
            String cardLine = bufferedReader.readLine();
            while (cardLine != null) {
                Matcher matcher = pattern.matcher(cardLine);
                if (matcher.matches()) {
                    this.addCards(
                            new Card(this.decodeSpaces(matcher.group(1)),
                                    Integer.parseInt(matcher.group(2)), Integer.parseInt(matcher.group(3))), 1);
                    cardLine = bufferedReader.readLine();
                }
                else {
                    TextPrinter.pausedPrint("There was an error loading the game");
                    TextPrinter.pausedPrint("Please contact whoever created this crap code");
                    if (this.fullDeck.size() == 0) {
                        this.addCards(new Card("Error Loading, No Cards Found", 0, 0), 1);
                    }
                    return false;
                }
            }
            TextPrinter.pausedPrint(Script.LOADED);
            reader.close();
            return true;
        } else {
            TextPrinter.pausedPrint(Script.NO_LOAD_POSSIBLE);
            reader.close();
            return false;
        }
    }

    /**
     * Takes a string and returns the string with all `'s turned into spaces.
     *
     * @param original the string with `'s
     * @return the string with spaces
     */
    private String decodeSpaces(String original) {
        return original.replaceAll("`", " ");
    }

    /**
     * Requests a starting deck and name from the user for the hero.
     */
    public void standardBuilder() {
        // Gets the hero's name and deck type from the user, creating the hero object from those.
        this.setNewName();
        this.chooseStartingDeck();
        System.out.println("");
    }

    /**
     * Takes a number between 0 and 2 and gives a beginning deck of cards to the hero according to that choice.
     */
    private void chooseStartingDeck() {
        // Informs the hero of the choice.
        TextPrinter.requestPrint(Script.DECK_REQUEST);
        // Gets the hero's choice.
        int choice = UserInputGetter.getNumberInput(0, 3);

        // Gives the hero a berserker deck.
        if (choice == 0) {
            this.addCards(new Card("Rush", 4, 0), 4);
            this.addCards(new Card("Balance", 2, 2), 2);
            // Gives the hero a tactician deck.
        } else if (choice == 1) {
            this.addCards(new Card("Rush", 3, 0), 2);
            this.addCards(new Card("Balance", 2, 2), 2);
            this.addCards(new Card("Guard", 0, 3), 2);
            // Gives the hero a tank deck.
        } else {
            this.addCards(new Card("Guard", 0, 4), 4);
            this.addCards(new Card("Balance", 2, 2), 2);
        }
    }

    /**
     * Takes a string from the user and sets it as the hero's name.
     */
    private void setNewName() {
        TextPrinter.requestPrint(Script.NAME_REQUEST);
        this.name = UserInputGetter.getStringInput();
    }

    /**
     * Compares a target value to the hero's progress value, returning whether they're equal.
     *
     * @param targetProgress the progress to compare to
     * @return true for equal, false if not
     */
    public boolean compareProgress(int targetProgress) {
        return targetProgress == this.progress;
    }

    /**
     * Changes the hero's progress value to match a passed value.
     *
     * @param newProgress the new progress value
     */
    public void setProgress(int newProgress) {
        this.progress = newProgress;
    }

    /**
     * Increases the hero's progress by one.
     */
    public void increaseProgress() {
        this.progress++;
    }
}
