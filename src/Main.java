public class Main {
    public static void main(String[] args) throws Exception {
        TextPrinter.pausedPrint("This is the base package for TBAGGOE games.");
        TextPrinter.pausedPrint("This file isn't meant to be run.");
        TextPrinter.pausedPrint("For developers, this contains every base class needed to make a mod of the game.");
        TextPrinter.pausedPrint("Check the git repository for more details.");
        TextPrinter.pausedPrint("For players, this jar may be needed to run a mod of the game.");
        TextPrinter.pausedPrint("Check with the mod developer to see if they packaged this file into their mod.");
        TextPrinter.pausedPrint("Buh-bye now.");
    }
}
